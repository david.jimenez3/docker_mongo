# Prerequisitos

GIT: Software de versionamiento necesario para actualizar el proyecto. [Link de descarga.](https://github.com/git-for-windows/git/releases/download/v2.45.0.windows.1/Git-2.45.0-64-bit.exe)

Docker Desktop (windows) software para desplegar el contenedro del proyecto. [Link de descarga.](https://www.docker.com/products/docker-desktop/)

Mongo Compas DBMS para mongo. [Link Descarga](https://downloads.mongodb.com/compass/mongodb-compass-1.42.5-win32-x64.exe) NOTA: es solo el visor de la base de datos, la base de datos como tal se despliega dentro del contenedor.

# Configuración y puesta en funcionamiento

## GIT

### Se recomienda configurar el [acceso ssh al repositorio GIT](https://docs.gitlab.com/ee/user/ssh.html), a fin de no solicitar contraseñas en cada peticion push / pull / fecth

Una vez instalado GIT:

Realizar clone al repositorio (configurado ssh):

```
git clone git@gitlab.com:david.jimenez3/docker_mongo.git
```
O si no tiene configurado ssh:
```
git clone https://gitlab.com/david.jimenez3/docker_mongo.git
```

## DOCKER
Instalar [Docker](https://docs.docker.com/engine/install/ubuntu/) en ubuntu.

Instalar [Docker](https://docs.docker.com/desktop/install/windows-install/) en windows.

Abrir la carpeta del proyecto mongo_docker que se generó al clonarlo y ejecutar el comando docker-compose up:
```
cd docker_mongo
docker-compose up
```
Una ves terminada la generación del contenedor, en su entorno local, puede acceder al sitio web [http://localhost:8081](http://localhost:8081) .

Ingresar el usuario y contraseña web definidos en las variables de entorno, estas se encuentran en el archivo .env del proyecto.
```
ME_CONFIG_BASICAUTH_USERNAME
ME_CONFIG_BASICAUTH_PASSWORD
```

### MONGO COMPASS (windows)
Al abrir Mongo Compass ingresar los datos de conexion así:

![Conexion_mongo_compass](img_readme/compass.png)

Los datos de  usuario y contraseña estan definidos en las variables de entorno, estas se encuentran en el archivo .env del proyecto.
```
MONGO_INITDB_ROOT_USERNAME
MONGO_INITDB_ROOT_PASSWORD
```

Al seleccionar "Save & connect" Se le solicitará un nombre de conexión para agregar a favoritos y luego podra visualizar la base de datos de prueba "testdb" en la parte izquierda. 

![Conexion_mongo_compass](img_readme/compass1.png)
#!/bin/bash

echo "############## INICIO importar datos ###################"
mongoimport --db testdb --collection users --file /tmp/testdb.users.json --jsonArray --username root --password root_pass --authenticationDatabase admin
echo "############## FIN importar datos ###################"